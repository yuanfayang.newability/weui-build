/**
 * Created by CH-yfy on 16/7/19.
 */

var gulp = require('gulp');
var concat = require('gulp-concat');
var header = require('gulp-header');
var connect = require('gulp-connect');
var less = require('gulp-less');
var autoprefixer = require('gulp-autoprefixer');
var ejs = require('gulp-ejs');
var uglify = require('gulp-uglify');
var ext_replace = require('gulp-ext-replace');
var cssmin = require('gulp-cssmin');

var pkg = require('./package.json');

var banner = '/***********this is my trainning*********************/\n';


gulp.task('js', function (cb) {
    count = 0;
    var end = function () {
        count++;
        if (count >= 3) {
            console.log("尝试执行回调第[" + count + "]次")
            cb()
        };
    };
    gulp.src(['./src/js/test.js'])
        .pipe(concat({path: 'test.min.js'}))
        .pipe(gulp.dest('./dist/js/'))
        .on('end', end);
});

gulp.task('ejs', function () {

    gulp.src(['./htmls/*.html', '!./htmls/_*.html'])
        .pipe(ejs({}))
        .pipe(gulp.dest('./dist/htmls'));
});

gulp.task('less', function () {

    gulp.src(['./src/less/main.less'])
        .pipe(less())
        .pipe(autoprefixer())
        .pipe(header(banner))
        .pipe(gulp.dest('./dist/css'))
});

gulp.task('cssmin',['less'],function () {
   gulp.src(['./dist/css/*.css','!./dist/css/*.min.css'])
       .pipe(cssmin())
       .pipe(header(banner))
       .pipe(ext_replace('.min.css'))
       .pipe(gulp.dest('./dist/css/'))
});

gulp.task('train', function () {
    console.log("gulp task try");
});

gulp.task('server', function () {
    connect.server();
});